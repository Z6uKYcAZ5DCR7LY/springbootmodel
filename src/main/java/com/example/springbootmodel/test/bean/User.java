package com.example.springbootmodel.test.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @program: springbootmodel
 * @description:
 * @author: yang
 * @version: 1.0
 * @create: 2020/11/23 18:09
 **/
@Setter
@Getter
@ToString
@ApiModel("用户实体")
public class User {

    @ApiModelProperty("用户名")
    public String username;
    @ApiModelProperty("密码")
    public String password;

}

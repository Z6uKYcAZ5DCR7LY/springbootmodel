package com.example.springbootmodel.test.action;

import com.example.springbootmodel.Utils.SpringContextUtil;
import com.example.springbootmodel.config.yml.ConfigYml;
import com.example.springbootmodel.test.bean.User;
import com.example.springbootmodel.test.service.TestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @program: springbootmodel
 * @description: 测试接口
 * @author: yang
 * @version: 1.0
 * @create: 2020/11/21 16:32
 **/
@Slf4j
@Api(tags = "测试")
@ResponseBody
@Scope("prototype")
@Controller("testAction")
@RequestMapping(value = "/test")
public class TestAction {

    @Resource(name = "testService")
    private TestService testService;

    @ResponseBody
    @RequestMapping(value="/testLog")
    public String testLog(HttpServletRequest request){
        log.info("异常：");
        log.debug("异常：");
        log.error("异常：");
    	return "测试日志";
    }

    @ApiOperation("测试接口")
    @PostMapping(value="/test")
    public String test(){
        testService.test();
        SpringContextUtil.getBean(TestService.class);
        System.out.println("server = " + ConfigYml.getProperty("server.port"));
        System.out.println("name = " + ConfigYml.getProperty("project.name"));
        System.out.println("listMap = " + ConfigYml.getConfigYmlObject().listMap.toString());
        return "yml";
    }

    @ApiOperation("获取用户接口")
    @GetMapping("/getUser")
    public User getUser(@ApiParam("用户名") String username,@ApiParam("密码")String password){
        User user = new User();
        user.setPassword(password);
        user.setUsername(username);
        return user;
    }

}

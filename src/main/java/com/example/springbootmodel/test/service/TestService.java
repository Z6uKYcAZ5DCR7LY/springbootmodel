package com.example.springbootmodel.test.service;

/**
 * @program: springbootmodel
 * @description:
 * @author: yang
 * @version: 1.0
 * @create: 2020/11/21 16:38
 **/
public interface TestService {

    String test();

}

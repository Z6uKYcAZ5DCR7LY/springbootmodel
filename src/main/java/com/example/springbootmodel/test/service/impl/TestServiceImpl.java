package com.example.springbootmodel.test.service.impl;

import com.example.springbootmodel.test.service.TestService;
import org.springframework.stereotype.Service;

/**
 * @program: springbootmodel
 * @description:
 * @author: yang
 * @version: 1.0
 * @create: 2020/11/21 16:38
 **/
@Service("testService")
public class TestServiceImpl implements TestService {

    @Override
    public String test(){
        return "测试";
    }
}


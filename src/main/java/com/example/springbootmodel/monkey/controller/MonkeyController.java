package com.example.springbootmodel.monkey.controller;

import com.cxytiandi.encrypt.springboot.annotation.Decrypt;
import com.cxytiandi.encrypt.springboot.annotation.Encrypt;
import com.example.springbootmodel.monkey.pojo.UserDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: springbootmodel
 * @description: 接口加密测试
 * @author: yang
 * @version: 1.0
 * @create: 2021/9/8 15:08
 **/
@Slf4j
@Api(tags = "")
@Controller("monkeyController")
@Scope("prototype")
@ResponseBody
@RequestMapping(value = "/monkey")
public class MonkeyController {

    //@Encrypt
    //@Decrypt(decyptParam = "id,name")
    @Decrypt()
    @GetMapping("/encryptStr")
    public String encryptStr(String name) {
        System.out.println(name);
        return "加密字符串";
    }


    @PostMapping("/encryptStr")
    public String encryptStr2(String name) {
        System.out.println(name);
        return "加密字符串";
    }

    @Encrypt
    @GetMapping("/encryptEntity")
    public UserDto encryptEntity() {
        UserDto dto = new UserDto();
        dto.setId(1);
        dto.setName("加密实体对象");
        return dto;
    }

    @Encrypt
    @GetMapping("/encryptEntity2/{id}")
    public UserDto encryptEntity2(@PathVariable int id) {
        UserDto dto = new UserDto();
        dto.setId(id);
        dto.setName("加密实体对象");
        return dto;
    }

    @Encrypt
    @Decrypt
    //@DecryptIgnore
    //@EncryptIgnore
    @PostMapping("/save")
    public UserDto save(@RequestBody UserDto dto) {
        System.err.println(dto.getId() + "\t" + dto.getName());
        return dto;
    }

    @Decrypt
    @PostMapping("/save/{id}")
    public UserDto save(@RequestBody UserDto dto, @PathVariable int id) {
        System.err.println(dto.getId() + "\t" + dto.getName());
        return dto;
    }

    @Encrypt
    @GetMapping("/save/{id}")
    public UserDto getUser(@PathVariable int id) {
        UserDto dto = new UserDto();
        dto.setId(id);
        dto.setName("加密实体对象");
        return dto;
    }

}

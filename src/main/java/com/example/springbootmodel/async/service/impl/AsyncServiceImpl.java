package com.example.springbootmodel.async.service.impl;

import com.example.springbootmodel.async.service.AsyncService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @program: springbootmodel
 * @description:
 * @author: yang
 * @version: 1.0
 * @create: 2021/6/14 23:59
 **/
@Slf4j
@Service("asyncService")
public class AsyncServiceImpl implements AsyncService {

    @Async
    @Override
    public void test(){
        try {
            Thread.sleep(10000);
            System.out.println("测试异步！");
        } catch (InterruptedException e) {
            log.error("异步测试异常：",e);
        }
    }

}

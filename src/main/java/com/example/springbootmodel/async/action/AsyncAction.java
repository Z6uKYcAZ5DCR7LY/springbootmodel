package com.example.springbootmodel.async.action;

import com.example.springbootmodel.async.service.AsyncService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: springbootmodel
 * @description: 异步
 * @author: yang
 * @version: 1.0
 * @create: 2021/6/14 23:44
 **/
@Slf4j
@Api(tags = "异步")
@Controller("asyncAction")
@Scope("prototype")
@ResponseBody
@RequestMapping(value = "/async")
public class AsyncAction {

    @Autowired
    private AsyncService asyncService;

    @ApiOperation(value = "异步测试")
    @RequestMapping(value="/asyncTest")
    public String asyncTest(HttpServletRequest request){
        asyncService.test();
    	return "ok";
    }

}

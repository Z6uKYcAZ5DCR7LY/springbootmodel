package com.example.springbootmodel;

import com.cxytiandi.encrypt.springboot.annotation.EnableEncrypt;
import com.example.springbootmodel.Utils.RuntimeUtil;
import com.example.springbootmodel.config.yml.ConfigYml;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

//@MapperScan("com.example.springbootmodel.mybatis.mapper")
//@ComponentScan(basePackages = {"com.swsk"})
//@EntityScan(basePackages = {"com.swsk"})
//@EnableJpaRepositories(basePackages = {"com.swsk"})
@EnableEncrypt // 开启接口加密
@EnableAsync // 开启异步注解功能
@EnableScheduling // 开启基于注解的定时任务
@SpringBootApplication
public class SpringbootmodelApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootmodelApplication.class, args);
        openHome();
    }

    //通过浏览器打开主页
    public static void openHome(){
        String port = ConfigYml.getProperty("server.port");
        String contextPath = ConfigYml.getProperty("server.servlet.context-path");
        String loginHtml = ConfigYml.getProperty("project.html.loginHtml");
        String url = "cmd   /c   start   http://localhost:" + port + contextPath + loginHtml;
        RuntimeUtil.excuteCmd(url,null);
    }

}

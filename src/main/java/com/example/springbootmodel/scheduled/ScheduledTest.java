package com.example.springbootmodel.scheduled;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @program: springbootmodel
 * @description: 定时任务测试
 * @author: yang
 * @version: 1.0
 * @create: 2021/6/15 0:44
 **/
@Controller
public class ScheduledTest {

    //秒   分   时     日   月   周几
    //0 * * * * MON-FRI
    //注意cron表达式的用法；
    @Scheduled(cron = "0/2 * * * * ? ")
    public void hello(){
        System.out.println("hello........");
    }

}

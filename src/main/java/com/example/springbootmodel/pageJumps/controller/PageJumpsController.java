package com.example.springbootmodel.pageJumps.controller;

import com.example.springbootmodel.config.webmvc.BaseAction;
import com.example.springbootmodel.config.yml.ConfigYml;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * @program: springbootmodel
 * @description: 页面跳转
 * @author: yang
 * @version: 1.0
 * @create: 2021/8/23 17:54
 **/
@Slf4j
@Controller("pageJumpsController")
@ResponseBody
@RequestMapping(value = "/pageJumps")
public class PageJumpsController extends BaseAction {

    /***
     * @description: 重定向
     * @param: url 跳转页面地址
     * @return: void
     * @author: ym
     * @date: 2021/8/23 18:04
     */
    @RequestMapping(value = "/sendRedirect",method = RequestMethod.GET)
    public void sendRedirect(String url){
        try {
            response.sendRedirect(request.getContextPath() + url);
        } catch (IOException e) {
            log.error("重定向页面异常：",e);
        }
    }

    /***
     * @description: 转发
     * @param: url
     * @return: void
     * @author: ym
     * @date: 2021/8/23 18:05
     */
    @RequestMapping(value = "/requestDispatcher",method = RequestMethod.GET)
    public void requestDispatcher(String url){
        try {
            request.getRequestDispatcher(url).forward(request,response);
        } catch (Exception e) {
            log.error("转发页面异常：",e);
        }
    }

}

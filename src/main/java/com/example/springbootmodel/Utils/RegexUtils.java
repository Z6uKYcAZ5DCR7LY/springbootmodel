package com.example.springbootmodel.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @program: MavenTest
 * @description: 正则表达工具
 * @author: yang
 * @version: 1.0
 * @create: 21/12/17 11:09
 **/
public class RegexUtils {

    /***
     * @description: 匹配截取(默认第一个)
     * @param: regex
    		   input
     * @return: java.lang.String
     * @author: ym
     * @date: 22/8/12 16:03
     */
    public static String intercept(String regex,String input){
        return intercept(regex, input, 0);
    }

    /***
     * @description: 匹配截取
     * @param: regex
    		   input
    		   frequency 位置
     * @return: java.lang.String
     * @author: ym
     * @date: 22/8/12 15:55
     */
    public static String intercept(String regex,String input,int frequency){
        List<String> strings = interceptByList(regex,input);
        if(strings.size() == 0){
            return null;
        }
        return strings.get(frequency);
    }

    /***
     * @description: 获取匹配的数组
     * @param: regex
    		   input
     * @return: java.util.List<java.lang.String>
     * @author: ym
     * @date: 22/8/12 16:01
     */
    public static List<String> interceptByList(String regex, String input){
        Matcher matcher = Pattern.compile(regex).matcher(input);
        List<String> strings = new ArrayList<>();
        while(matcher.find()){
            strings.add(matcher.group(0));
        }
        return strings;
    }

    /**
     * 通过正则表达式判断字符串是否为数字
     * @param str
     * @return
     */
    public static boolean isNumber(String str) {
        // 通过Matcher进行字符串匹配
        // 如果正则匹配通过 m.matches() 方法返回 true ，反之 false
        return Pattern.compile("-?[0-9]+(\\\\.[0-9]+)?").matcher(str).matches();
    }

}

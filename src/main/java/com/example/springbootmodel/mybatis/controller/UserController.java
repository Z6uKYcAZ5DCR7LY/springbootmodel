package com.example.springbootmodel.mybatis.controller;

import com.example.springbootmodel.mybatis.mapper.UserMapper;
import com.example.springbootmodel.mybatis.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/mybatis")
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @GetMapping("/queryUserList")
    public List<User> queryUserList(){
        return userMapper.queryUserList();
    }

    @GetMapping("/queryUserById")
    public User queryUserById(){
        return userMapper.queryUserById(1);
    }

    @GetMapping("/addUser")
    public int addUser(){
        return userMapper.addUser(new User(9,"mybatis boot add","213131"));
    }

    @GetMapping("/updateUser")
    public int updateUser(){
        return userMapper.updateUser(new User(8,"mybatis boot up","213131"));
    }

    @GetMapping("/deleteUser")
    public int deleteUser(){
        return userMapper.deleteUser(7);
    }

}

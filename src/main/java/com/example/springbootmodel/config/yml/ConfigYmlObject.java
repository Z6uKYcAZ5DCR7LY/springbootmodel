package com.example.springbootmodel.config.yml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @program: springbootmodel
 * @description: yml复杂类型配置
 * @author: yang
 * @version: 1.0
 * @create: 2020/11/21 17:34
 **/
@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "project")
public class ConfigYmlObject {

    public List<Map<String,String>> listMap;

}

package com.example.springbootmodel.config.yml;

import com.example.springbootmodel.Utils.SpringContextUtil;
import org.springframework.core.env.Environment;

/**
 * @program: springbootmodel
 * @description: YML文件读取
 * @author: yang
 * @version: 1.0
 * @create: 2020/11/21 17:13
 **/
public class ConfigYml {

    public static final Environment ENVIRONMENT = SpringContextUtil.getBean(Environment.class);
    private static final ConfigYmlObject CONFIG_YML_OBJECT = SpringContextUtil.getBean(ConfigYmlObject.class);

    /***
     * @description: 读取yml文件配置
     * @param: key
     * @return: java.lang.String
     * @author: ym
     * @date: 2020/11/21 17:16
     */
    public static String getProperty(String key){
        return ENVIRONMENT.getProperty(key);
    }

    /***
     * @description: 读取yml文件配置，同时添加默认
     * @param: key
    		   defaultValue
     * @return: java.lang.String
     * @author: ym
     * @date: 2020/11/21 17:17
     */
    public static String getProperty(String key,String defaultValue){
        return ENVIRONMENT.getProperty(key,defaultValue);
    }

    /***
     * @description: 读取yml文件配置，同时添加返回类型（只能返回基本类型）
     * @param: key
    		   tClass
     * @return: T
     * @author: ym
     * @date: 2020/11/21 17:18
     */
    public static <T> T getProperty(String key, Class<T> tClass){
        return ENVIRONMENT.getProperty(key,tClass);
    }

    /***
     * @description: 返回复杂类型对象
     * @param:
     * @return: com.example.springbootmodel.config.yml.ConfigYmlObject
     * @author: ym
     * @date: 2020/11/21 17:38
     */
    public static ConfigYmlObject getConfigYmlObject(){
        return CONFIG_YML_OBJECT;
    }

}

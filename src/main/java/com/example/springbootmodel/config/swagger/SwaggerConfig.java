package com.example.springbootmodel.config.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;

/**
 * @program: springbootmodel
 * @description: swagger配置类
 * @author: yang
 * @version: 1.0
 * @create: 2020/11/23 17:12
 **/
@Configuration
@EnableOpenApi
public class SwaggerConfig {

    /***
     * @description: 主要作用是构建我们接口文档的页面显示的基础信息
     * @param:
     * @return: springfox.documentation.service.ApiInfo
     * @author: ym
     * @date: 2020/11/23 17:18
     */
    private ApiInfo apiInfo(){
        Contact contact = new Contact("杨明", "https://www.cnblogs.com/tioxy/", "1341960937@qq.com");
        return new ApiInfo(
                "标题",
                "项目描述",
                // 版本
                "1.0",
                // 服务条款网址
                "https://www.cnblogs.com/tioxy/",
                // 项目联系人
                contact,
                // 执照
                "Apache 2.0",
                // 执照地址
                "http://www.apache.org/licenses/LICENSE-2.0",
                // 供应商扩展
                new ArrayList());
    }

    /*@Bean
    public Docket docket(){
        return new Docket(DocumentationType.OAS_30).apiInfo(apiInfo());
    }*/

    /***
     * @description: 配置扫描接口及开关（可以配置多个）
     * @param: environment
     * @return: springfox.documentation.spring.web.plugins.Docket
     * @author: ym
     * @date: 2020/11/23 17:31
     */
    @Bean
    public Docket docket(Environment environment){
        // 设置显示的Swagger环境(yml指定的读取环境)
        Profiles dev = Profiles.of("project");
        // 获取项目环境
        boolean flag = environment.acceptsProfiles(dev);
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                // 配置Api文档分组
                .groupName("接口分组-1")
                // enable()是否启用Swagger，默认是true
                .enable(flag)
                .select()
                // RequestHandlerSelectors,配置要扫描接口的方式
                // basePackage指定要扫描的包
                // any()扫描所有，项目中的所有接口都会被扫描到
                // none()不扫描
                // withClassAnnotation()扫描类上的注解
                // withMethodAnnotation()扫描方法上的注解
                .apis(RequestHandlerSelectors.basePackage("com.example.springbootmodel"))
                // paths()过滤某个路径
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public Docket docket2(Environment environment){
        // 设置显示的Swagger环境(yml指定的读取环境)
        Profiles dev = Profiles.of("project");
        // 获取项目环境
        boolean flag = environment.acceptsProfiles(dev);
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                // 配置Api文档分组
                .groupName("接口分组-2")
                // enable()是否启用Swagger，默认是true
                .enable(flag)
                .select()
                // RequestHandlerSelectors,配置要扫描接口的方式
                // basePackage指定要扫描的包
                // any()扫描所有，项目中的所有接口都会被扫描到
                // none()不扫描
                // withClassAnnotation()扫描类上的注解
                // withMethodAnnotation()扫描方法上的注解
                .apis(RequestHandlerSelectors.basePackage("com.example.springbootmodel"))
                // paths()过滤某个路径
                .paths(PathSelectors.any())
                .build();
    }


}

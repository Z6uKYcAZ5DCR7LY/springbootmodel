package com.example.springbootmodel.config.interceptor;

import com.example.springbootmodel.config.yml.ConfigYml;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @program: springbootmodel
 * @description: 使用WebMvcConfigurerAdapter可以来扩展SpringMVC的功能
 *               WebMvcConfigurerAdapter 2.0以前过时
 *               WebMvcConfigurer 2.0以后
 * @author: yang
 * @version: 1.0
 * @create: 2020/11/26 20:06
 **/
@Configuration
public class MyMvcConfig implements WebMvcConfigurer {

    /*@Autowired
    private StaticPagePathFinder staticPagePathFinder;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //addViewControllers可以方便的实现一个请求直接映射成视图，而无需书写controller
        //registry.addViewController("请求路径").setViewName("请求页面文件路径")
        try {
            for (StaticPagePathFinder.PagePaths pagePaths : staticPagePathFinder.findPath()) {
                String urlPath = pagePaths.getUrlPath();
                registry.addViewController(urlPath + ".html").setViewName(pagePaths.getFilePath());
                if (!urlPath.isEmpty()) {
                    registry.addViewController(urlPath).setViewName(pagePaths.getFilePath());
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("无法定位静态页面:" + e.getMessage(), e);
        }
    }*/

    @Value("${project.interceptor.intercept}")
    private String intercept;

    @Value("${project.interceptor.release}")
    private String release;

    @Value("${server.servlet.context-path}")
    private String contextPath;

    @Value("${project.mappesAddress}")
    private String mappesAddress;

    /***
     * @description: 注册拦截器
     * @param: registry
     * @return: void
     * @author: ym
     * @date: 2021/8/23 17:49
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginHandlerInterceptor()) // 路径判断
                .addPathPatterns(ConfigYml.getProperty("project.interceptor.intercept", "/**")) // 拦截路径
                .excludePathPatterns(release.split(",")); // 放行路径
    }

    /***
     * @description: 内置tomcat虚拟文件映射路径
     *               在此处配置的虚拟路径，用springboot内置的tomcat时有效，用外部的tomcat也有效;
     *               所以用到外部的tomcat时不需在tomcat/config下的相应文件配置虚拟路径了,阿里云linux也没问题
     * @param: registry
     * @return: void
     * @author: ym
     * @date: 2020/11/28 17:36
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // Windows下 访问：http://localhost:端口号/项目名/项目名_files...
        registry.addResourceHandler( contextPath + "_files/**") // 添加访问映射名称
                .addResourceLocations("file:" + mappesAddress); // 添加映射目录地址，以"/"结尾
        // Mac或Linux下(没有CDEF盘符)
        // registry.addResourceHandler("/uploads/**").addResourceLocations("file:/Users/liuyanzhao/Documents/uploads/");
    }

    /***
     * @description: 解决跨域问题
     * @param: registry
     * @return: void
     * @author: ym
     * @date: 23/9/5 15:00
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS")
                // 当allowCredentials设置为true时，allowedOrigins不能包含特殊值"*"，
                // 因为无法在"Access-Control-Allow-Origin"响应头中设置该值
                //.allowCredentials(true)
                .maxAge(3600)
                .allowedHeaders("*");
    }

}

package com.example.springbootmodel.config.webmvc;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @program: springbootmodel
 * @description:
 * @author: yang
 * @version: 1.0
 * @create: 2021/8/23 18:01
 **/
@Controller("baseAction")
@Scope("prototype")
public class BaseAction {

    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected HttpSession session;

    public BaseAction() {
    }

    @ModelAttribute
    public void setWebResources(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
        this.response.setCharacterEncoding("UTF-8");
    }

}
